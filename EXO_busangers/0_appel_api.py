import json
import requests
from pprint import pprint
from datetime import datetime as dt
"""
 EXO 1
 Faire un appel à l'api et
 stoquer le resultat dans un
 fichier .json
"""
def exo1():
    print("EXO1 "+"-"*20)

    URL = "https://data.angers.fr/api/"\
          "records/1.0/search/"
    params = {
        "dataset":"bus-tram-position-tr",
        "rows":10
    }
    resp = requests.get(URL, params=params)
    data = resp.json()

    pprint(data)
    date = dt.now().strftime('%Y%m%d_%H%M')
    with open(f'./output/out_{date}.json', 'wt') as f:
        json.dump(data, f)
    
if __name__ == "__main__":
    exo1()
