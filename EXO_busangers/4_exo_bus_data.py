import json
import requests
import argparse
import logging
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from dateutil.parser import parse
from glob import glob
from pprint import pprint

sns.set(style="whitegrid")
log = logging.getLogger(__name__)
handler = logging.StreamHandler()
log.setLevel(logging.DEBUG)
log.addHandler(handler)


def load_data(directory, loadonly=None):
    log.info(f"loading data from {directory}")
    # Permettra de stocker tous les df,
    # afin de les concaténer
    liste_df = []

    # Permettont de stocker les dates et les nhits,
    # afin d'en faire un df 
    # spécifique
    liste_dates = []
    liste_nb = []

    loaded_files = 0
    
    # Création d'un dataframe pour chaque json
    files = glob(directory + "/*.json")[:]
    for index, file in enumerate(files):
        print(f"{index}/{len(files)}")
        with open(file, 'r') as json_file:
            try:
                data = json.load(json_file)
                dft = pd.DataFrame(
                    [bus['fields'] for bus in
                     data['records']])

                # On récupère la date 
                # 1. On coupe sur l'expression trams_ et
                # on récupère la partie droite
                s = file.split('trams_')[1]
                # 2. On coupe sur les points '.' et on
                # récupère la partie gauche
                s = s.split('.')[0]
                # 3. On transforme en date
                date = dt.datetime.strptime(
                    s, '%Y-%m-%dT%H_%M_%S')
                dft['date'] = date
            
                # On stock le df
                liste_df.append(dft)
                
                # On stock la date et le nombre de bus
                # à cet instant
                liste_dates.append(date)
                liste_nb.append(data['nhits'])
                loaded_files += 1

                if loadonly is not None and \
                   loaded_files >= loadonly:
                    break
            # Certains json sont vides, on les passe
            except Exception as e:
                print(e,json_file.name)
                continue
    
    # On concatène tous les df afin d'avoir
    # toutes les infos dans un seul 
    # et même endroit
    df = pd.concat(liste_df)

    # On créé la série du nombre de bus pour
    # une date donnée
    df_nb = pd.DataFrame(
        data=liste_nb,
        index=liste_dates,
        columns=['Nb_bus']
    )

    return (df, df_nb)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "dir",
        help="Directory where json data are.")
    parser.add_argument(
        "-l",
        type=int,
        help='Load only N files')
    
    args = parser.parse_args()
    
    (detail_df, nbbus_df) = load_data(
        args.dir, loadonly=args.l)

    detail_df.info()
    nbbus_df.info()
