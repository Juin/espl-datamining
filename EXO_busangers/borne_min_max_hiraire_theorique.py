import csv
import json
from pprint import pprint


def get_min_max():
    """
    Recherche des valeurs min et max des horaire
    pour chaque stop_id du fichier stop_times.txt
    """
    
    result = {}
    line = 0
    with open("stop_times.txt", 'rt') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            line += 1
            if line == 1:
                continue
            time_as_float = float(row[1].replace(":", ""))
            if row[3] not in result:
                result[row[3]] = {
                    "min":time_as_float,
                    "max":time_as_float
                }
            if result[row[3]]["min"] > time_as_float:
                result[row[3]]["min"] = time_as_float
            if result[row[3]]["max"] < time_as_float:
                result[row[3]]["max"] = time_as_float
                
    return result


def map_stop_key(my_dict):
    """
    Modification des cle du dictionnaire passe en parametre
    de stop_id vers stop_code (source stops.txt)
    """
    line = 0
    mapping = {}

    # --  mapping stop_id,stop_code a partir du fichier stops.txt
    with open("stops.txt", 'rt') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            line += 1
            if line == 1:
                continue
            mapping[row[0]] = row[1]
    
    result = {}

    # -- creation d'un nouveau dictionnaire avec la cle stop_code
    for key, val in my_dict.items():
        print(key)
        
        result[mapping[key]] = val
    return result
    


if __name__ == "__main__":

    min_max_by_stop_id = get_min_max()
    #pprint(min_max_by_stop_id)

    min_max_by_stop_id = map_stop_key(min_max_by_stop_id)
    #pprint(min_max_by_stop_id)
    
    with open("output.json", 'wt') as out:
        json.dump(min_max_by_stop_id, out, indent=2)

    print("file has been dumped to ./output.json")
