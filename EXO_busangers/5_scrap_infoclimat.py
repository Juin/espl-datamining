import argparse
import requests
import os
import locale
import unicodedata
import re
import json
from time import sleep
from datetime import datetime as dt, timedelta as td
from lxml import etree, html
from pprint import pprint

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
DIRECTORY = "./info_climat_pages"

def grab_store_result(start_from, stop_at, limit=None, wait=5):
    # URL to grab
    #https://www.infoclimat.fr/observations-meteo/archives/
    #1er/aout/2019/angers-beaucouze/07230.html
    
    current_date = start_from
    index = 0
    while current_date <= stop_at:       
        day = current_date.day
        if day == 1:
            day = "1er"
        month = current_date.strftime('%B')
        month = unicodedata.normalize('NFD', month)
        month = month.encode('ascii', 'ignore')
        month = month.decode("utf-8")
        #print(day, month)
        url = "https://www.infoclimat.fr/observations-meteo/archives/"
        url += "%s/%s/2019/angers-beaucouze/07230.html" % (
            day, month
        )
        print(url)
        resp = requests.get(url)

        str_date = current_date.strftime("%Y%m%d")
        with open("%s/%s.html" % (DIRECTORY, str_date), "wt") as out:
            out.write(resp.text)
        
        if limit is not None and index+1 >= limit:
            break
        sleep(5)
        current_date += td(days=1)
        index += 1
        

def toscv():
    print("to scv")
    err = 0
    parser = etree.HTMLParser()
    lines = []
    for file_name in os.listdir(DIRECTORY):
        print(file_name)
        with open('%s/%s' % (DIRECTORY, file_name), 'rt') as html_file:
            #tree = etree.parse(html_file, parser)
            tree = html.parse(html_file)
            print(tree)
            r = tree.xpath("/html/body/div[8]/div[4]/div/div[4]/table/tbody/tr")
            for elem in r:
                hour = elem.xpath("./th/span/text()")[0]
                spans = elem.xpath("./td")
                print(len(spans))
                #print(spans)
                #print(texts)
                text_spans = []
                for s in spans:
                    #print("    '%s'" % s.text_content().replace("\n"," ").strip())
                    text_spans.append(s.text_content().replace("\n"," ").strip())
                pprint(text_spans)
                lst = '&'.join(text_spans)
                print(lst)
                if len(spans) == 10:
                    m = re.match("^(?:.*)&(?:(.*)°C.*)&(?:.*)&(?:(.*) .*)&(?:(.*)km/hraf\.(.*))&(.*)%&(.*)&(.*)&(?:([0-9|\.]*).*)&(?:(.*) .*)$", lst)
                else:
                    m = re.match("^(?:.*)&(?:(.*)°C.*)&(?:(.*) .*)&(?:(.*)km/hraf\.(.*))&(.*)%&(.*)&(.*)&(?:([0-9|\.]*).*)&(?:(.*) .*)$", lst)
                print(m)
                if m is not None:
                    temp = float(m.group(1))
                    pressure = float(m.group(8))
                    prec = float(m.group(2))
                    wind = float(m.group(3))
                    wind_raf = float(m.group(4))
                else:
                    err += 1
                    raise Exception("A")
                str_date = "%s %s" % (file_name, hour)
                #m = re.match(spans[0]"")
                data = {
                    "date": dt.strptime(str_date, "%Y%m%d.html %Hh").isoformat(), 
                    "temp": temp,
                    "pressure": pressure,
                    "prec": prec,
                    "wind": wind,
                    "wind_raf": wind_raf
                }
                lines.append(data)
                if True:#file_name == "20191121.html":#data["prec"] > 0:
                    print(data)
    with open("weather.json", 'wt') as out:
        json.dump(lines, out, sort_keys=True, indent=2)

    print("err ", err)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help='subcommand',  dest="CMD")
    subparsers.required = True
    scrap = subparsers.add_parser(
        'scrap', help='scrap infoclimat')
    scrap.add_argument(
        "--limit", "-l",
        type=int,
        help='Load only N files')
    
    tocsv = subparsers.add_parser(
        'toscv', help='tranform scrpped html to scv files')
    args = parser.parse_args()
    print(args)
    
    if args.CMD == "scrap":
        grab_store_result(dt(2019, 8, 1), dt(2019, 12, 31), limit=args.limit)
    elif args.CMD == "toscv":
        toscv()
