"""
#Lancez une pièce 100 fois et vérifier combien vous avez de "pile".
#Pour cela vous pourrez utiliser :
#  la fonction np.sum qui s'applique à un NumPy Array (ou à une liste).
#   ou bien la sélection par condition du type mon_array >= 3.2

"""
import numpy as np
from pprint import pprint

if __name__ == '__main__':
    ll = np.random.binomial(
        1,0.5,100)
    # -- nombre de pile
    print(np.sum(ll))

    # -- probabilité que pile soit tiré
    # 50 foi 

    nb_experiences = 10000
    nb_succes = 0
    for i in range(nb_experiences):
        ll = np.random.binomial(
            1,0.5,100)
        nb_piles = np.sum(ll)
        if nb_piles == 50:
            nb_succes += 1

    print(f"Proba: {nb_succes/nb_experiences}")
        
   
 
