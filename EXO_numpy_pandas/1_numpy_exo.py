#Calcul IMC
#    Max : 1m76 et 82kg,
#    Sophie : 1m63 et 68kg,
#    Franck : 1m92 et 110 kg.

#Il se calcule simplement en
#divisant le poids (en kg) par le carré de la taille (m). Un IMC normal se situe entre 18,5 et 25. 
import numpy as np

if __name__ == "__main__":

    data = [
        [1.76, 82, 'Max'],
        [1.63, 68, 'Sophie'],
        [1.92, 110, 'Frabck']
    ]

    for size, weight, name in data:
        print(name, size, weight)
        imc = weight/size**2
        print(f"    imc = {imc}")

    test1 = [el[0] for el in data]

    ma_liste = []
    for el in data:
        ma_liste.append(el[0])

    sizes = np.array([el[0] for el in data])
    weights = np.array([el[1] for el in data])
    print(sizes)
    print(weights)

    
    print(weights/sizes**2)

    
