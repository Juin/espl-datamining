# Evaluation ESPL 2021-03-19

## Questions générales

* Quelles librairies python connaissez vous pour resoudre des problèmes statistique ou fabriquer des modèles statistiques connaissez vous?
* Quels outils statistiques permettant de créer un model connaissez vous?
* Qu'est ce qu'un histogramme?

## Charger les données

Vous disposez des donnée des PIB
* https://donnees.banquemondiale.org/indicateur/NY.GDP.MKTP.CD?view=chart
* https://api.worldbank.org/v2/fr/indicator/NY.GDP.MKTP.CD?downloadformat=csv

Charger les données pour afficher un tableau partiel

## Générer un graphe d'évolution du PIB par pays pour les 5 pays avec le plus haut PIB

## Générer un graphe d'évolution du PIB par pays pour les 5 pays avec le moins haut PIB

## Charger les donnée de causes de mortalité

Data source https://www.who.int/data/data-collection-tools/who-mortality-database:
* Country code: https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/mort_country_codes.zip?sfvrsn=800faac2_5&ua=1
* https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/morticd10_part1.zip?sfvrsn=e2a4f93a_9&ua=1
* https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/morticd10_part2.zip?sfvrsn=6e55000b_3&ua=1
* https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/morticd10_part3.zip?sfvrsn=9f1111a2_3&ua=1
* https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/morticd10_part4.zip?sfvrsn=259c5c23_10&ua=1
* https://cdn.who.int/media/docs/default-source/world-health-data-platform/mortality-raw-data/morticd10_part5.zip?sfvrsn=ad970d0b_13&ua=1

### Afficher le tableau partiellement

### Afficher un tableau classant les pays par nombre de mort total

## Joindre des données
Source: https://github.com/kamillamagna/ICD-10-CSV
ICD-10 code: https://raw.githubusercontent.com/kamillamagna/ICD-10-CSV/master/categories.csv

Ajouter la description de la cause
Afficher sous forme de bar-graph les 20 premiere cause de mortalité

## Ajouter l'information nombre d'habitant par pays en deduire le taux de mortalité
Source: https://donnees.banquemondiale.org/indicator/SP.POP.TOTL?end=2019&start=1960&view=chart

## Determiner si il y a correlation entre le taux de mortalité et le PIB

## Afficher une regression linéaire

## Afficher les coefficients de la regression linéaires
