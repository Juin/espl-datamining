# Cours data-mining ESPL Q1/2021

## Introduction th�orique

### Support de cours

* https://gitlab.com/Juin/espl-datamining
* Data-mining: cours-data-mining.pdf
* Th�orie sur les regressions lin�aire: cours-theorie-regression-lineraire.pdf

### M�thodologie r�sum�e

* formalisation des objectifs
* acquisition des donn�es
* pr�paration des donn�es
* apprentissage & application des m�thodes
* interpr�tation & explication
* �valuation et validation
* d�ploiement

## Introduction � Numpy, matplotlib, seaborn et Pandas

* prendre en main ces outils et savoir utiliser la documentations.
* le support de cours se trouve dans EXO_numpy_pandas

## Exercice bus angers 

* Voir notebook dans EXO_busangers
* Source horaire temps r�el: https://cdn.aeon-creation.com/ESPL/angers-bus-trams.7z
* Charger les donn�es
* Explorer les donn�es
* Afficher les donn�es sous forme de graphique avec aggregats
  (nb_bus/jours, ecart_moyen/jours ...)
* Observer des "patern" pour definir des "classes"
  (Periode temporelle vacances, semaine, weekend)
* Obtenir un mod�le sous forme d'une regression lin�aire pour
  determiner un retard possible dans l'avenire
* Nettoyer les donn�es (suppression des valeurs ab�rantes,
  utilisation de irigo_gtfs.zip)
  Voir script python borne_min_max_hiraire_theorique.py
  Doc format GTFS https://developers.google.com/transit/gtfs/examples/gtfs-feed
* V�rifier l'influence de la m�t�o sur le retard des bus

## Exercice donn�e siret

* Voir repertoire EXO_siret
* Source de donn�e
* utilisation de Dash
* Cr�er un outils de data-vis bas� sur les donn�es siret
* Afficher nombre soci�t� par d�partement
* Afficher histogramme code Naf/nb soci�t�
* Afficher histogramme d�partement/nb soci�t�
* Ajouter un filtre s�lection d�partement
* Ajouter un filtre s�lection code Naf

## Evaluation

